package com.mastercard.cas.ds.acs.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastercard.cas.ds.acs.ACSFeatureToggle;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.dto.BaseAResDTO;
import com.mastercard.cas.ds.serialize.MessageDeSerializer;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Component("acs.feature.enabled")
@Slf4j
public class ACSFeatureToggleEnabled implements ACSFeatureToggle {
	@Value("${acs.areq.uri}")
	private String acsAreqUri;

	@Autowired
	private WebClient acsWebClient;

	@Autowired
	private MessageDeSerializer messageDeSerializer;

	@Override
	public BaseAResDTO processAReq(BaseAReqDTO areq) {
		log.info("Authentication request received. sending it to ACS.");
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			String requestBody = objectMapper.writeValueAsString(areq);
			String responseBody = null;
			responseBody = acsWebClient.post().uri(acsAreqUri)
					.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
					.body(Mono.just(requestBody), String.class).retrieve().bodyToMono(String.class).block();

			return messageDeSerializer.deserializeAres(responseBody, areq.getMessageVersion());
		} catch (Exception e) {
			log.error("Error occured while making acs request.", e);
			throw new RuntimeException("ACS Exception", e);
		}
	}
}
