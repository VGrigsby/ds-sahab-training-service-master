package com.mastercard.cas.ds.acs;

import org.ff4j.aop.Flip;

import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.dto.BaseAResDTO;

public interface ACSFeatureToggle {
	@Flip(name = "acs", alterBean = "acs.feature.enabled")
	BaseAResDTO processAReq(BaseAReqDTO areq);

}
