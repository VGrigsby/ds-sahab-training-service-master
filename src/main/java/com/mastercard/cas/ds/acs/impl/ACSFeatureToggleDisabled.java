package com.mastercard.cas.ds.acs.impl;

import org.springframework.stereotype.Component;

import com.mastercard.cas.ds.acs.ACSFeatureToggle;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.dto.BaseAResDTO;
import com.mastercard.cas.ds.dto.v2_1_0.AResDTO;

@Component("acs.feature.disabled")
public class ACSFeatureToggleDisabled implements ACSFeatureToggle {
    @Override
    public BaseAResDTO processAReq(BaseAReqDTO areq) {
    	AResDTO aresDTO = new AResDTO();
		aresDTO.setAcctNumber("5472000000005001");
		aresDTO.setAcsOperatorID("ACS_RSA_020100_00011");
		aresDTO.setAcsTransID("1111eedc-3671-4595-8b7f-f06264c9e08b");
		aresDTO.setAuthenticationValue("01");
		aresDTO.setDsReferenceNumber("DS_MAST_020100_00038");
		aresDTO.setMessageType("ARes");
		aresDTO.setMessageVersion(areq.getMessageVersion());
		aresDTO.setThreeDSServerTransID("3333eedc-3671-4595-8b7f-f06264c9e08a");
		aresDTO.setTransStatus("U");
		aresDTO.setDsTransID("2222eedc-3671-4595-8b7f-f06264c9e08a");
		aresDTO.setEci("01");
		return aresDTO;
    }
}