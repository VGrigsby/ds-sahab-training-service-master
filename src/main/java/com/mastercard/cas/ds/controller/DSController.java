package com.mastercard.cas.ds.controller;

import java.io.IOException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mastercard.cas.ds.dto.AReqError;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.dto.BaseAResDTO;
import com.mastercard.cas.ds.serialize.MessageDeSerializer;
import com.mastercard.cas.ds.service.DSService;
import com.mastercard.cas.ds.validation.MessageValidator;
import com.mastercard.enterprise.error.handling.core.http.errors.Errors;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/3ds")
@Tag(name = "authentication")
public class DSController {

	@Autowired
	private MessageValidator messageValidator;

	@Autowired
	private DSService dsService;

	@Autowired
	private MessageDeSerializer messageDeSerializer;

	@PostMapping(value = "/processAReq", consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "Process Authentication Request")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "OK", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = BaseAResDTO.class)) }),
			@ApiResponse(responseCode = "400", description = "Bad Request", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Errors.class)) }),
			@ApiResponse(responseCode = "403", description = "Forbidden", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Errors.class)) }),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Errors.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Errors.class)) }) })
	public Object process(@RequestBody String payload) throws IOException, IllegalAccessException {
		log.debug("Received AREQ", payload);

		BaseAReqDTO aRequest = null;
		aRequest = messageDeSerializer.deserializeAreq(payload);
		String dsTransID = UUID.randomUUID().toString();
		aRequest.setDsTransID(dsTransID);
		AReqError error = messageValidator.validateAreq(aRequest);
		if (error != null) {
			return error;
		}

		BaseAResDTO baseAResDTO = dsService.processAreq(aRequest);
		log.debug("Returning Success response for account {}.", aRequest.getAcctNumber());
		return baseAResDTO;
	}
}
