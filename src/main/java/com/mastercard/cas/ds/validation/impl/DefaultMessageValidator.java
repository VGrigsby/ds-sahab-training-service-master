package com.mastercard.cas.ds.validation.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mastercard.cas.ds.common.AReqMessageVersion;
import com.mastercard.cas.ds.dto.AReqError;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.validation.AreqValidator;
import com.mastercard.cas.ds.validation.MessageValidator;
import com.mastercard.cas.ds.validation.MessageValidatorFactory;

import lombok.extern.slf4j.Slf4j;

@Component("defaultMessageValidator")
@Slf4j
public class DefaultMessageValidator implements MessageValidator {

	@Autowired
	private MessageValidatorFactory messageValidatorFactory;

	public AReqError validateAreq(BaseAReqDTO areq) {
		log.debug("Validating Areq request for message version:{} and accountNumber:{}", areq.getMessageVersion(),
				areq.getAcctNumber());
		AreqValidator areqMessageValidator = messageValidatorFactory
				.getAreqMessageValidator(AReqMessageVersion.valueOfReqVersion(areq.getMessageVersion()));
		return areqMessageValidator.validate(areq);
	}

}
