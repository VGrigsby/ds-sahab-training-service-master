package com.mastercard.cas.ds.validation.field.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.mastercard.cas.ds.dto.AReqError;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.validation.field.FieldValidator;

@Component
public class DefaultAccountFieldValidator implements FieldValidator {

	@Override
	public AReqError validate(BaseAReqDTO req) {
		if (StringUtils.isBlank(req.getAcctNumber())) {
			AReqError areqError = new AReqError();
			areqError.setErrorCode("201");
			areqError.setErrorDescription("Required element missing");
			areqError.setErrorDetail("acctNumber");
			areqError.setErrorMessageType("AReq");
			areqError.setErrorComponent("D");
			areqError.setDsTransID(req.getDsTransID());
			areqError.setMessageType("Erro");
			return areqError;

		}
		return null;
	}

}
