package com.mastercard.cas.ds.validation.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mastercard.cas.ds.common.AReqMessageVersion;
import com.mastercard.cas.ds.dto.AReqError;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.validation.AreqValidator;
import com.mastercard.cas.ds.validation.field.FieldValidator;
import com.mastercard.cas.ds.validation.field.impl.DefaultAccountFieldValidator;
import com.mastercard.cas.ds.validation.field.impl.DefaultThreeDsRefNumberValidator;

@Service("defaultAreqValidator")
public class DefaultAreqValidator implements AreqValidator {

	@Autowired
	private DefaultAccountFieldValidator defaultAccountFieldValidator;
	@Autowired
	private DefaultThreeDsRefNumberValidator defaultThreeDsRefNumberValidator;

	@Override
	public AReqError validate(BaseAReqDTO baseAReqDTO) {
		Set<FieldValidator> fieldValidators = getFieldValidators(
				AReqMessageVersion.valueOfReqVersion(baseAReqDTO.getMessageVersion()));

		for (FieldValidator validator : fieldValidators) {
			AReqError aReqError = validator.validate(baseAReqDTO);
			if (aReqError != null) {
				return aReqError;
			}

		}
		return null;
	}

	@Override
	public Set<FieldValidator> getFieldValidators(AReqMessageVersion messageVersion) {
		Set<FieldValidator> fieldValidators = new HashSet<FieldValidator>();
		fieldValidators.add(defaultAccountFieldValidator);
		fieldValidators.add(defaultThreeDsRefNumberValidator);
		return fieldValidators;
	}

}
