package com.mastercard.cas.ds.validation;

import java.util.Set;

import com.mastercard.cas.ds.common.AReqMessageVersion;
import com.mastercard.cas.ds.dto.AReqError;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.validation.field.FieldValidator;

public interface AreqValidator {
	AReqError validate(BaseAReqDTO baseAReqDTO);

	Set<FieldValidator> getFieldValidators(AReqMessageVersion messageVersion);
}
