package com.mastercard.cas.ds.validation.field.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mastercard.cas.ds.dto.AReqError;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.entity.EmvcoRefNumDetails;
import com.mastercard.cas.ds.service.ThreeDsRefrenceNumberDetailsService;
import com.mastercard.cas.ds.validation.field.FieldValidator;

@Component
public class DefaultThreeDsRefNumberValidator implements FieldValidator {

	@Autowired
	private ThreeDsRefrenceNumberDetailsService threeDsRefrenceNumberDetailsService;

	@Override
	public AReqError validate(BaseAReqDTO req) {
		EmvcoRefNumDetails emvcoRefNumDetails = threeDsRefrenceNumberDetailsService
				.getEmvcoRefNumDetails(req);

		if (emvcoRefNumDetails == null) {
			AReqError aReqError = new AReqError();
			aReqError.setErrorCode("303");
			aReqError.setErrorDescription("Access denied, invalid endpoint");
			aReqError.setErrorDetail("threeDSServerRefNumber not recognized");
			aReqError.setErrorMessageType("AReq");
			aReqError.setErrorComponent("D");
			aReqError.setMessageType("Erro");
			aReqError.setDsTransID(req.getDsTransID());
			return aReqError;

		}
		return null;
	}

}
