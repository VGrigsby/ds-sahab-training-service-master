package com.mastercard.cas.ds.validation.field;

import com.mastercard.cas.ds.dto.AReqError;
import com.mastercard.cas.ds.dto.BaseAReqDTO;

public interface FieldValidator {
	AReqError validate(BaseAReqDTO req);

}
