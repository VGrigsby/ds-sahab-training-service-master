package com.mastercard.cas.ds.validation;

import org.springframework.stereotype.Service;

import com.mastercard.cas.ds.dto.AReqError;
import com.mastercard.cas.ds.dto.BaseAReqDTO;

@Service
public interface MessageValidator {
	AReqError validateAreq(BaseAReqDTO areq);

}
