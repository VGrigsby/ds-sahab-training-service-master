package com.mastercard.cas.ds.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.mastercard.cas.ds.common.AReqMessageVersion;

@Component
public class MessageValidatorFactory {

	@Autowired
	@Qualifier("defaultAreqValidator")
	private AreqValidator defaultAreqValidator;

	public AreqValidator getAreqMessageValidator(AReqMessageVersion messageVersion) {
		switch (messageVersion) {
		case V_2_1_0:
			return defaultAreqValidator;
		default:
			return defaultAreqValidator;

		}
	}

	

}
