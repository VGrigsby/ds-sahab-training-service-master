package com.mastercard.cas.ds.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mastercard.cas.ds.common.DSConstants;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.entity.EmvcoRefNumDetails;
import com.mastercard.cas.ds.repository.EmvcoRefNumDetailsRepo;
import com.mastercard.cas.ds.service.ThreeDsRefrenceNumberDetailsService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DefaultThreeDsRefrenceNumberDetailsService implements ThreeDsRefrenceNumberDetailsService {

	@Autowired
	private EmvcoRefNumDetailsRepo emvcoRefNumDetailsRepo;

	@Override
	public EmvcoRefNumDetails getEmvcoRefNumDetails(BaseAReqDTO areq) {
		EmvcoRefNumDetails refDetails = emvcoRefNumDetailsRepo
				.findByEmvcoRefNumAndProviderTypeCode(areq.getThreeDSServerRefNumber(), DSConstants.SER_PROVIDER_TYPE);
		log.info("3ds Ref details for {} found in databse: {} ", areq.getThreeDSServerRefNumber(), refDetails != null);

		return refDetails;
	}

}
