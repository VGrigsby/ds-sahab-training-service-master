package com.mastercard.cas.ds.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.mastercard.cas.ds.acs.ACSFeatureToggle;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.dto.BaseAResDTO;
import com.mastercard.cas.ds.service.DSService;

@Component
public class DefaultDSService implements DSService {

	@Autowired
	@Qualifier("acs.feature.disabled")
	private ACSFeatureToggle acsFeatureToggle;

	public BaseAResDTO processAreq(BaseAReqDTO areq) {

		return acsFeatureToggle.processAReq(areq);
	}

}
