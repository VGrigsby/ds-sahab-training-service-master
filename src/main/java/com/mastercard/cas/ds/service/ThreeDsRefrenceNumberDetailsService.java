package com.mastercard.cas.ds.service;

import org.springframework.cache.annotation.Cacheable;

import com.mastercard.cas.ds.config.CachingConfig;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.entity.EmvcoRefNumDetails;

public interface ThreeDsRefrenceNumberDetailsService {

	@Cacheable(cacheNames = CachingConfig.DS_CACHE, key = "#areq.threeDSServerRefNumber")
	EmvcoRefNumDetails getEmvcoRefNumDetails(BaseAReqDTO areq);

}
