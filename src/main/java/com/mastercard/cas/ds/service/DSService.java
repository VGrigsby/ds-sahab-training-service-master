package com.mastercard.cas.ds.service;

import org.springframework.stereotype.Service;

import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.dto.BaseAResDTO;

@Service
public interface DSService {

	BaseAResDTO processAreq(BaseAReqDTO areq);

}
