package com.mastercard.cas.ds.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseAResDTO {
	private String acctNumber;
	private String acsTransID; 
	private String acsOperatorID;
	private String authenticationValue;
	private String dsReferenceNumber;
	private String dsTransID;
	private String eci;
	private String messageType;
	private String messageVersion;
	private String sdkTransID;
	private String threeDSServerTransID;
	private String transStatus;

}
