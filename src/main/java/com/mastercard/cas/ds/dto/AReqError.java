package com.mastercard.cas.ds.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AReqError {
	private String errorCode;
	private String errorDescription;
	private String errorDetail;
	private String errorMessageType;
	private String errorComponent;
	private String messageType;
	private String dsTransID;
}
