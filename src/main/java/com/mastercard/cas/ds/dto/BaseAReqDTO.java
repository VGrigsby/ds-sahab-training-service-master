package com.mastercard.cas.ds.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(content = Include.NON_NULL, value = Include.NON_NULL)
public class BaseAReqDTO {
	private String acctNumber;
	private String deviceChannel;
	private String messageCategory;
	private String messageType;
	private String messageVersion;
	private String sdkTransID;
	private String threeDSServerTransID;
	private String threeDSServerRefNumber;
	private String whiteListStatus;
	private String threeDSRequestorID;
	private String dsTransID;
	private String dsReferenceNumber;

}
