package com.mastercard.cas.ds.dto.v1_1_0;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mastercard.cas.ds.dto.BaseAResDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class AResDTO extends BaseAResDTO{}
