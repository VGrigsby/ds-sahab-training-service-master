package com.mastercard.cas.ds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DsSahabTrainingServiceApplication {

	public static void main(String[] args) {
			SpringApplication.run(DsSahabTrainingServiceApplication.class, args);
	}

}
