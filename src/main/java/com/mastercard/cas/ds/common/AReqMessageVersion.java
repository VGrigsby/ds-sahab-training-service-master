package com.mastercard.cas.ds.common;

public enum AReqMessageVersion {
	V_1_1_0("1.1.0"), V_2_1_0("2.1.0");

	private String version;

	public String getVersion() {
		return version;
	}

	AReqMessageVersion(String version) {
		this.version = version;
	}

	public static AReqMessageVersion valueOfReqVersion(String version) {
		for (AReqMessageVersion v : values()) {
			if (v.name().equalsIgnoreCase(version)) {
				return v;
			} else if (v.getVersion().equalsIgnoreCase(version)) {
				return v;
			}
		}

		throw new RuntimeException("No enum defined for " + version);
	}
}
