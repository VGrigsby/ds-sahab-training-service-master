package com.mastercard.cas.ds.config;


import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.tags.Tag;

@Configuration
@OpenAPIDefinition
@Profile({"default", "local", "dev", "bdd"})
public class OpenApiConfiguration {
	@Bean
	public OpenAPI customOpenAPI(Environment env) {
		Map<String, Object> extensions = new HashMap<>();
		extensions.put("x-artifactId", "x-device-authentication-services");
		Contact contact = new Contact().email("sahab.singh@mastercard.com").name("CAS - China On Soil")
				.extensions(extensions);
		
		Tag serviceTag = new Tag().name("ds-sahab-training-service")
				.description("Operations related to DS Training Service binding");
		
		License license = new License().name("Apache License Version 2.0")
				.url(env.getProperty("http://www.apache.org/licenses/LICENSE-2.0"));
		
		Info seerviceInfo = new Info().title("DS Training Service").description("DS Training Service")
				.termsOfService(env.getProperty("")).license(license).contact(contact).version("1.0.0");

		return new OpenAPI().components(new Components())
				.addTagsItem(serviceTag)
				.info(seerviceInfo);
	}

}
