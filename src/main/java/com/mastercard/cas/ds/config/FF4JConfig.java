package com.mastercard.cas.ds.config;

import java.io.IOException;
import java.io.InputStream;

import org.ff4j.FF4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

@Configuration
public class FF4JConfig {

	@Autowired
	@Qualifier("defaultResourceLoader")
	private ResourceLoader resourceLoader;

	@Value("${feature.toggle.xml.config}")
	private String xmlConfig;

	@Bean
	public FF4j getFF4j() throws IOException {
		Resource banner = resourceLoader.getResource(xmlConfig);
		InputStream in = banner.getInputStream();
		FF4j ff4j = new FF4j(in);
		ff4j.audit(false);
		return ff4j;
	}
}