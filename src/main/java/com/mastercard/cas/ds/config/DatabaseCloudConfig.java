package com.mastercard.cas.ds.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

import com.zaxxer.hikari.HikariDataSource;

@Profile({ "!local" })
@Configuration
public class DatabaseCloudConfig {
    @Value("${db.username}")
    private String username;
 
    @Value("${db.password}")
    private String password;
 
    @Value("${db.url}")
    private String url;
 
    @Value("${db.driver-class-name}")
    private String driverClassName;
 
    @Value("${db.max-pool-size}")
    private int maxPoolSize;
 
    @Value("${db.min-pool-size}")
    private int minPoolSize;
 
    @Value("${db.pool-name}")
    private String poolName;
 
    @Bean
    @Primary
    public DataSource datasource(Environment env) {
        HikariDataSource ds = new HikariDataSource();
        ds.setUsername(username);
 
        ds.setPassword(password);
        ds.setPassword(env.getProperty(convertToHashicorpLabel()));
        if (ds.getPassword() == null) {
            throw new RuntimeException("Datasource password is null");
        }
        ds.setJdbcUrl(url);
        ds.setDriverClassName(driverClassName);
        ds.setMaximumPoolSize(maxPoolSize);
        ds.setMinimumIdle(minPoolSize);
        ds.setPoolName(poolName);
        return ds;
    }
    private String convertToHashicorpLabel() {
        return username + "_label";
    }
}
