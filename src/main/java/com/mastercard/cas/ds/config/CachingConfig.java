package com.mastercard.cas.ds.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableCaching
public class CachingConfig {

	public static final String DS_CACHE = "ds_cache";

	@Bean
	public CacheManager cacheManager() {
		return new ConcurrentMapCacheManager(DS_CACHE);
	}

	@Scheduled(fixedRate = 10000)
	public void evictCacheValue() {
		cacheManager().getCache(DS_CACHE).clear();
	}
}