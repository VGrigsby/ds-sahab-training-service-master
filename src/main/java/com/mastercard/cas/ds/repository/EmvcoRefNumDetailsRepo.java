package com.mastercard.cas.ds.repository;

import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.mastercard.cas.ds.entity.EmvcoRefNumDetails;

@Repository
public interface EmvcoRefNumDetailsRepo
		extends JpaRepository<EmvcoRefNumDetails, String>, JpaSpecificationExecutor<EmvcoRefNumDetails> {
	@NewSpan("repository.EMVCOReferenceNumber.findByEmvcoRefNumAndProviderTypeCode")
	EmvcoRefNumDetails findByEmvcoRefNumAndProviderTypeCode(String emvcoRefNum, String providerTypeCode);
}
