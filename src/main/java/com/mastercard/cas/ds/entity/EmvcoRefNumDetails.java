package com.mastercard.cas.ds.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "EMVCO_REF_NUM_DTL")
public class EmvcoRefNumDetails {
	@Id
	@Column(name = "EMVCO_REF_NUM", length = 50)
	private String emvcoRefNum;
	
	@Column(name = "STAT_SW", length = 1, columnDefinition = "CHAR")
	private String statusSwitch;
	
	@Column(name = "PRVDR_TYPE_CD", length = 3)
	private String providerTypeCode;
}
