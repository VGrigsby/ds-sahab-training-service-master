package com.mastercard.cas.ds.serialize;

import java.io.IOException;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastercard.cas.ds.common.AReqMessageVersion;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.dto.BaseAResDTO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MessageDeSerializer {

	public BaseAReqDTO deserializeAreq(String payload) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();

		Map<?, ?> readTree = objectMapper.readValue(payload, Map.class);
		String messageVersion = (String) readTree.get("messageVersion");
		log.debug("Deserializing Areq payload for message version: {}", messageVersion);

		switch (AReqMessageVersion.valueOfReqVersion(messageVersion)) {
		case V_2_1_0:
			return objectMapper.convertValue(readTree, com.mastercard.cas.ds.dto.v2_1_0.AReqDTO.class);
		default:
			return objectMapper.convertValue(readTree, BaseAReqDTO.class);

		}

	}

	public BaseAResDTO deserializeAres(String payload, String messageVersion) throws IOException {
		log.debug("Deserializing Ares payload for message version: {}", messageVersion);
		ObjectMapper objectMapper = new ObjectMapper();
		Map<?, ?> readTree = objectMapper.readValue(payload, Map.class);
		switch (AReqMessageVersion.valueOfReqVersion(messageVersion)) {
		case V_2_1_0:
			return objectMapper.convertValue(readTree, com.mastercard.cas.ds.dto.v2_1_0.AResDTO.class);
		default:
			return objectMapper.convertValue(readTree, BaseAResDTO.class);

		}
	}

}
