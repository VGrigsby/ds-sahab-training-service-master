package com.mastercard.cas.ds.serialize;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import com.mastercard.cas.ds.constant.AreqConstants;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.dto.BaseAResDTO;

public class MessageDeSerializerTest {

	@Test
	public void testDeserialise() throws IOException {
		MessageDeSerializer messageDeSerializer = new MessageDeSerializer();
		BaseAReqDTO baseAReqDTO = messageDeSerializer.deserializeAreq(AreqConstants.AREQ_PAYLOAD);
		assertNotNull(baseAReqDTO);
	}

	@Test
	public void deserializeAres() throws IOException {
		MessageDeSerializer messageDeSerializer = new MessageDeSerializer();
		BaseAResDTO baseAResDTO = messageDeSerializer.deserializeAres(AreqConstants.ACS_REP, "2.1.0");
		assertNotNull(baseAResDTO);
	}

}
