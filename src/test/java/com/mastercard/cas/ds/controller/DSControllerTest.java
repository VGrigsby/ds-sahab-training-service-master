package com.mastercard.cas.ds.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastercard.cas.ds.constant.AreqConstants;
import com.mastercard.cas.ds.dto.AReqError;
import com.mastercard.cas.ds.dto.v2_1_0.AReqDTO;
import com.mastercard.cas.ds.dto.v2_1_0.AResDTO;
import com.mastercard.cas.ds.serialize.MessageDeSerializer;
import com.mastercard.cas.ds.service.DSService;
import com.mastercard.cas.ds.validation.MessageValidator;

@ExtendWith(MockitoExtension.class)
public class DSControllerTest {
	

	@InjectMocks
	private DSController controller;
	@Mock
	private MessageValidator messageValidator;

	@Mock
	private DSService dsService;

	@Mock
	private MessageDeSerializer messageDeSerializer;

	@Test
	public void process_success() throws IllegalAccessException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		Map<?, ?> readTree = objectMapper.readValue(AreqConstants.AREQ_PAYLOAD, Map.class);
		AReqDTO aReqDTO = objectMapper.convertValue(readTree, com.mastercard.cas.ds.dto.v2_1_0.AReqDTO.class);

		Mockito.when(messageDeSerializer.deserializeAreq(Mockito.any())).thenReturn(aReqDTO);
		Mockito.when(messageValidator.validateAreq(Mockito.any())).thenReturn(null);
		Mockito.when(dsService.processAreq(Mockito.any())).thenReturn(buildAres());
		AResDTO resp = (AResDTO) controller.process(AreqConstants.AREQ_PAYLOAD);
		assertNotNull(resp);
		assertEquals("5472000000005001", resp.getAcctNumber());

	}

	@Test
	public void process_failure() throws IllegalAccessException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		Map<?, ?> readTree = objectMapper.readValue(AreqConstants.AREQ_PAYLOAD_MISSING_ACC, Map.class);
		AReqDTO aReqDTO = objectMapper.convertValue(readTree, com.mastercard.cas.ds.dto.v2_1_0.AReqDTO.class);

		Mockito.when(messageDeSerializer.deserializeAreq(Mockito.any())).thenReturn(aReqDTO);
		Mockito.when(messageValidator.validateAreq(Mockito.any())).thenReturn(getAccountMissingError());
		AReqError resp = (AReqError) controller.process(AreqConstants.AREQ_PAYLOAD);
		assertNotNull(resp);

	}

	private AResDTO buildAres() {
		AResDTO aresDTO = new AResDTO();
		aresDTO.setAcctNumber("5472000000005001");
		aresDTO.setThreeDSServerTransID("2222eedc-3671-4595-8b7f-f06264c9e08a");
		aresDTO.setAcsOperatorID("ACS_RSA_020100_00011");
		aresDTO.setAcsTransID("1111eedc-3671-4595-8b7f-f06264c9e08b");
		aresDTO.setAuthenticationValue("01");
		aresDTO.setDsReferenceNumber("DS_MAST_020100_00038");
		aresDTO.setMessageType("AReq");
		aresDTO.setMessageVersion("2.1.0");
		aresDTO.setThreeDSServerTransID("3333eedc-3671-4595-8b7f-f06264c9e08a");
		aresDTO.setTransStatus("Y");
		aresDTO.setEci("01");
		return aresDTO;

	}

	private AReqError getAccountMissingError() {
		AReqError areqError = new AReqError();
		areqError.setErrorCode("201");
		areqError.setErrorDescription("Required element missing");
		areqError.setErrorDetail("acctNumber");
		areqError.setErrorMessageType("AReq");
		areqError.setErrorComponent("D");
		areqError.setDsTransID("3333eedc-3671-4595-8b7f-f06264c9e08a");
		areqError.setMessageType("Erro");
		return areqError;
	}
}
