package com.mastercard.cas.ds.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastercard.cas.ds.acs.ACSFeatureToggle;
import com.mastercard.cas.ds.constant.AreqConstants;
import com.mastercard.cas.ds.dto.BaseAResDTO;
import com.mastercard.cas.ds.dto.v2_1_0.AReqDTO;
import com.mastercard.cas.ds.service.impl.DefaultDSService;

@ExtendWith(MockitoExtension.class)
public class DefaultDSServiceTest {

	@InjectMocks
	private DefaultDSService service;

	@Mock
	private ACSFeatureToggle acsFeatureToggle;

	@Test
	public void processAreq_success() throws JsonMappingException, JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		Map<?, ?> readTree = objectMapper.readValue(AreqConstants.AREQ_PAYLOAD, Map.class);
		AReqDTO aRequest = objectMapper.convertValue(readTree, AReqDTO.class);
		BaseAResDTO respObj = new BaseAResDTO();
		respObj.setAcctNumber("5472000000005001");
		Mockito.when(acsFeatureToggle.processAReq(aRequest)).thenReturn(respObj);
		BaseAResDTO resp = service.processAreq(aRequest);
		assertNotNull(resp);
		assertEquals("5472000000005001", resp.getAcctNumber());
	}

}
