package com.mastercard.cas.ds.config;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

import io.swagger.v3.oas.models.OpenAPI;

@ExtendWith(MockitoExtension.class)
public class OpenApiConfigurationTest {

	@InjectMocks
	private OpenApiConfiguration openApiConfiguration;

	@Test
	public void customOpenAPI() {
		Environment env = Mockito.mock(Environment.class);
		OpenAPI api = openApiConfiguration.customOpenAPI(env);
		assertNotNull(api);
	}

}
