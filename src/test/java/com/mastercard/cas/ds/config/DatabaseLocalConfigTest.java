package com.mastercard.cas.ds.config;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.sql.DataSource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
public class DatabaseLocalConfigTest {

	@InjectMocks
	DatabaseLocalConfig databaseLocalConfig;

	@BeforeEach
	public void beforeAll() {

		ReflectionTestUtils.setField(databaseLocalConfig, "username", "testuser");
		ReflectionTestUtils.setField(databaseLocalConfig, "password", "testpass");
		ReflectionTestUtils.setField(databaseLocalConfig, "url", "testurl");
		ReflectionTestUtils.setField(databaseLocalConfig, "driverClassName", "oracle.jdbc.OracleDriver");
		ReflectionTestUtils.setField(databaseLocalConfig, "maxPoolSize", 1);
	}

	@Test
	public void datasource() {
		Environment env = Mockito.mock(Environment.class);
		
		DataSource dataSource = databaseLocalConfig.datasource(env);
		assertNotNull(dataSource);
	}

}
