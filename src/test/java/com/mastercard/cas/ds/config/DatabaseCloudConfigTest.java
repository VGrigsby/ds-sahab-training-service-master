package com.mastercard.cas.ds.config;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.sql.DataSource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
public class DatabaseCloudConfigTest {

	@InjectMocks
	DatabaseCloudConfig databaseCloudConfig;

	@BeforeEach
	public void beforeAll() {

		ReflectionTestUtils.setField(databaseCloudConfig, "username", "testuser");
		ReflectionTestUtils.setField(databaseCloudConfig, "password", "testpass");
		ReflectionTestUtils.setField(databaseCloudConfig, "url", "testurl");
		ReflectionTestUtils.setField(databaseCloudConfig, "driverClassName", "oracle.jdbc.OracleDriver");
		ReflectionTestUtils.setField(databaseCloudConfig, "maxPoolSize", 1);
	}

	@Test
	public void datasource() {
		Environment env = Mockito.mock(Environment.class);
		Mockito.when(env.getProperty("testuser_label")).thenReturn("testPas");
		
		DataSource dataSource = databaseCloudConfig.datasource(env);
		assertNotNull(dataSource);
	}

}
