package com.mastercard.cas.ds.config;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.ff4j.FF4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
public class FF4JConfigTest {

	@InjectMocks
	private FF4JConfig ff4JConfig;

	@Mock
	private ResourceLoader resourceLoader;

	@BeforeEach
	public void beforeAll() {
		ReflectionTestUtils.setField(ff4JConfig, "xmlConfig", "xmlConfig");
	}

	@Test
	public void getFF4j() throws IOException {
		Resource resource = Mockito.mock(Resource.class);
		Mockito.when(resourceLoader.getResource(Mockito.anyString())).thenReturn(resource);
		 File initialFile = new File("src/main/resources/feature-toggle.xml");
		    InputStream targetStream = new FileInputStream(initialFile);
		Mockito.when(resource.getInputStream()).thenReturn(targetStream);
		FF4j ff4j = ff4JConfig.getFF4j();
		assertNotNull(ff4j);
	}

}
