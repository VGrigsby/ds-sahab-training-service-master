package com.mastercard.cas.ds.config;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.cache.CacheManager;

public class CachingConfigTest {

	@Test
	public void cacheManager() {
		CachingConfig cachingConfig = new CachingConfig();
		CacheManager cacheManager = cachingConfig.cacheManager();
		assertNotNull(cacheManager);

	}

	@Test
	public void evictCacheValue() {
		CachingConfig cachingConfig = new CachingConfig();
		cachingConfig.evictCacheValue();

	}
}
