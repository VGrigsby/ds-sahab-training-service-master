package com.mastercard.cas.ds.config;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.net.ssl.SSLException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class WebClientConfigTest {
	@InjectMocks
	private WebClientConfig webClientConfig;
	
	@Test
	public void buildWebClient() throws SSLException {
		assertNotNull(webClientConfig.buildWebClient());
	}

}
