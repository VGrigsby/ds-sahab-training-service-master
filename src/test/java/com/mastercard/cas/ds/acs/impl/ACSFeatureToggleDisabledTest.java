package com.mastercard.cas.ds.acs.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.dto.BaseAResDTO;

@ExtendWith(MockitoExtension.class)
public class ACSFeatureToggleDisabledTest {

	@InjectMocks
	private ACSFeatureToggleDisabled acsFeatureToggleDisabled;

	@Test
	public void processAReqTest() {
		BaseAReqDTO baseAReqDTO = new BaseAReqDTO();
		BaseAResDTO baseAResDTO = acsFeatureToggleDisabled.processAReq(baseAReqDTO);
		assertNotNull(baseAResDTO);
	}

}
