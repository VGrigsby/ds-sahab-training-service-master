package com.mastercard.cas.ds.acs.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestBodySpec;
import org.springframework.web.reactive.function.client.WebClient.RequestBodyUriSpec;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;
import org.springframework.web.reactive.function.client.WebClient.ResponseSpec;

import com.mastercard.cas.ds.constant.AreqConstants;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.dto.BaseAResDTO;
import com.mastercard.cas.ds.serialize.MessageDeSerializer;

import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
public class ACSFeatureToggleEnabledTest {

	@InjectMocks
	private ACSFeatureToggleEnabled acsFeatureToggleEnabled;

	@Mock
	private WebClient acsWebClient;

	@Mock
	private MessageDeSerializer messageDeSerializer;

	@BeforeEach
	public void beforeAll() {
		ReflectionTestUtils.setField(acsFeatureToggleEnabled, "acsAreqUri", "SecureCode-ACS/3ds2uf");
	}

	@Test
	public void processAReqTest() throws IOException {
		RequestBodyUriSpec requestBodyUriSpec = Mockito.mock(RequestBodyUriSpec.class);
		Mockito.when(acsWebClient.post()).thenReturn(requestBodyUriSpec);
		RequestBodySpec requestBodySpec = Mockito.mock(RequestBodySpec.class);
		Mockito.when(requestBodyUriSpec.uri(Mockito.anyString())).thenReturn(requestBodySpec);
		Mockito.when(requestBodySpec.header(Mockito.anyString(), Mockito.anyString())).thenReturn(requestBodySpec);
		RequestHeadersSpec requestHeadersSpec = Mockito.mock(RequestHeadersSpec.class);
		Mockito.when(requestBodySpec.body(Mockito.any(Mono.class), Mockito.any(Class.class)))
				.thenReturn(requestHeadersSpec);
		ResponseSpec responseSpec = Mockito.mock(ResponseSpec.class);
		Mockito.when(requestHeadersSpec.retrieve()).thenReturn(responseSpec);
		Mono mono = Mockito.mock(Mono.class);
		Mockito.when(responseSpec.bodyToMono(Mockito.any(Class.class))).thenReturn(mono);
		Mockito.when(mono.block()).thenReturn(AreqConstants.ACS_REP);

		Mockito.when(messageDeSerializer.deserializeAres(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(new BaseAResDTO());

		BaseAReqDTO baseAReqDTO = new BaseAReqDTO();
		baseAReqDTO.setMessageVersion("2.1.0");
		BaseAResDTO baseAResDTO = acsFeatureToggleEnabled.processAReq(baseAReqDTO);
		assertNotNull(baseAResDTO);
	}

}
