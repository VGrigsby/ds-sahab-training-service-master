package com.mastercard.cas.ds.constant;

public abstract class AreqConstants {
	private AreqConstants() {
	}

	public static final String AREQ_PAYLOAD = "{\r\n" + "\"acctNumber\":\"5472000000005001\",\r\n"
			+ "\"deviceChannel\":\"01\",\r\n" + "\"messageCategory\":\"01\",\r\n" + "\"messageType\":\"AReq\",\r\n"
			+ "\"messageVersion\":\"2.1.0\",\r\n"
			+ "\"threeDSServerTransID\":\"0018eedc-3671-4595-8b7f-f06264c9e08a\",\r\n"
			+ "\"threeDSServerRefNumber\":\"3DS_LOA_SER_PPFU_020100_00008\",\r\n" + "\"whiteListStatus\":\"Y\"\r\n"
			+ "}";

	public static final String AREQ_PAYLOAD_MISSING_ACC = "{\r\n" + "\"deviceChannel\":\"01\",\r\n" + "\r\n"
			+ "\"messageCategory\":\"01\",\r\n" + "\r\n" + "\"messageType\":\"AReq\",\r\n" + "\r\n"
			+ "\"messageVersion\":\"2.1.0\",\r\n" + "\r\n"
			+ "\"threeDSServerTransID\":\"0018eedc-3671-4595-8b7f-f06264c9e08a\",\r\n" + "\r\n"
			+ "\"threeDSServerRefNumber\":\"3DS_LOA_SER_PPFU_020100_00008\",\r\n" + "\r\n"
			+ "\"whiteListStatus\":\"Y\"\r\n" + "\r\n" + "}\r\n" + "\r\n" + "";

	public static final String ACS_REP = "{\r\n" + "    \"acctNumber\": null,\r\n"
			+ "    \"acsTransID\": \"f97da378-7f80-44a6-9db5-eee7696c277b\",\r\n"
			+ "    \"acsOperatorID\": \"MCD-LOA-SRV-JDFH-V201-12345\",\r\n"
			+ "    \"authenticationValue\": \"xgTC8v0+AAAAAAAAAAAAAAAAAAAA\",\r\n"
			+ "    \"dsReferenceNumber\": \"EMVCoTrackUpto32EMVCoTrackUpto32-Dev\",\r\n"
			+ "    \"dsTransID\": \"ced2c7b0-e9d8-4e62-a714-7f53f8493ac9\",\r\n" + "    \"eci\": \"02\",\r\n"
			+ "    \"messageType\": \"ARes\",\r\n" + "    \"messageVersion\": \"2.1.0\",\r\n"
			+ "    \"sdkTransID\": \"0018eedc-3671-4595-8b7f-f06264c9e08a\",\r\n"
			+ "    \"threeDSServerTransID\": \"0018eedc-3671-4595-8b7f-f06264c9e08a\",\r\n"
			+ "    \"transStatus\": \"Y\"\r\n" + "}";
}
