package com.mastercard.cas.ds.validation.field.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastercard.cas.ds.constant.AreqConstants;
import com.mastercard.cas.ds.dto.AReqError;
import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.dto.v1_1_0.AReqDTO;
import com.mastercard.cas.ds.entity.EmvcoRefNumDetails;
import com.mastercard.cas.ds.service.ThreeDsRefrenceNumberDetailsService;

@ExtendWith(MockitoExtension.class)
public class DefaultThreeDsRefNumberValidatorTest {

	@InjectMocks
	private DefaultThreeDsRefNumberValidator defaultThreeDsRefNumberValidator;

	@Mock
	private ThreeDsRefrenceNumberDetailsService threeDsRefrenceNumberDetailsService;

	@Test
	public void validate() throws JsonMappingException, JsonProcessingException {

		ObjectMapper objectMapper = new ObjectMapper();
		Map<?, ?> readTree = objectMapper.readValue(AreqConstants.AREQ_PAYLOAD, Map.class);
		AReqDTO aReqDTO = objectMapper.convertValue(readTree, com.mastercard.cas.ds.dto.v1_1_0.AReqDTO.class);
		EmvcoRefNumDetails emvcoRefNumDetails = new EmvcoRefNumDetails();
		Mockito.when(threeDsRefrenceNumberDetailsService.getEmvcoRefNumDetails(Mockito.any(BaseAReqDTO.class)))
				.thenReturn(emvcoRefNumDetails);
		AReqError aReqError = defaultThreeDsRefNumberValidator.validate(aReqDTO);
		assertNull(aReqError);

		Mockito.when(threeDsRefrenceNumberDetailsService.getEmvcoRefNumDetails(Mockito.any(BaseAReqDTO.class)))
				.thenReturn(null);
		aReqError = defaultThreeDsRefNumberValidator.validate(aReqDTO);
		assertNotNull(aReqError);
	}

}
