package com.mastercard.cas.ds.validation.field.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Map;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastercard.cas.ds.constant.AreqConstants;
import com.mastercard.cas.ds.dto.AReqError;
import com.mastercard.cas.ds.dto.v1_1_0.AReqDTO;

public class DefaultAccountFieldValidatorTest {

	@Test
	public void validate() throws JsonMappingException, JsonProcessingException {
		DefaultAccountFieldValidator accountFieldValidator = new DefaultAccountFieldValidator();
		ObjectMapper objectMapper = new ObjectMapper();

		Map<?, ?> readTree = objectMapper.readValue(AreqConstants.AREQ_PAYLOAD, Map.class);
		AReqDTO aReqDTO = objectMapper.convertValue(readTree, com.mastercard.cas.ds.dto.v1_1_0.AReqDTO.class);
		AReqError aReqError = accountFieldValidator.validate(aReqDTO);
		assertNull(aReqError);

		aReqDTO.setAcctNumber(null);
		aReqError = accountFieldValidator.validate(aReqDTO);
		assertNotNull(aReqError);
	}

}
