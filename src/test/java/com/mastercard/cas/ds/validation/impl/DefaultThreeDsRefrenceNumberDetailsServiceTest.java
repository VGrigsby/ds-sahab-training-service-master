package com.mastercard.cas.ds.validation.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.mastercard.cas.ds.dto.BaseAReqDTO;
import com.mastercard.cas.ds.entity.EmvcoRefNumDetails;
import com.mastercard.cas.ds.repository.EmvcoRefNumDetailsRepo;
import com.mastercard.cas.ds.service.impl.DefaultThreeDsRefrenceNumberDetailsService;

@ExtendWith(MockitoExtension.class)
public class DefaultThreeDsRefrenceNumberDetailsServiceTest {

	@InjectMocks
	private DefaultThreeDsRefrenceNumberDetailsService defaultThreeDsRefrenceNumberDetailsService;

	@Mock
	private EmvcoRefNumDetailsRepo emvcoRefNumDetailsRepo;

	@Test
	public void getEmvcoRefNumDetails() {
		BaseAReqDTO aReqDTO = new BaseAReqDTO();
		aReqDTO.setThreeDSServerRefNumber("abc");
		Mockito.when(
				emvcoRefNumDetailsRepo.findByEmvcoRefNumAndProviderTypeCode("abc", "SER"))
				.thenReturn(new EmvcoRefNumDetails());
		EmvcoRefNumDetails emvcoRefNumDetails = defaultThreeDsRefrenceNumberDetailsService
				.getEmvcoRefNumDetails(aReqDTO);
		assertNotNull(emvcoRefNumDetails);
	}

}
