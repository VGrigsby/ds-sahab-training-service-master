package com.mastercard.cas.ds.validation.impl;

import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastercard.cas.ds.constant.AreqConstants;
import com.mastercard.cas.ds.dto.AReqError;
import com.mastercard.cas.ds.dto.v1_1_0.AReqDTO;
import com.mastercard.cas.ds.validation.field.impl.DefaultAccountFieldValidator;
import com.mastercard.cas.ds.validation.field.impl.DefaultThreeDsRefNumberValidator;

@ExtendWith(MockitoExtension.class)
public class DefaultAreqValidatorTest {

	

	@InjectMocks
	private DefaultAreqValidator defaultAreqValidator;

	@Mock
	private DefaultAccountFieldValidator defaultAccountFieldValidator;
	@Mock
	private DefaultThreeDsRefNumberValidator defaultThreeDsRefNumberValidator;

	@Test
	public void validate() throws JsonMappingException, JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		Map<?, ?> readTree = objectMapper.readValue(AreqConstants.AREQ_PAYLOAD, Map.class);
		AReqDTO aReqDTO = objectMapper.convertValue(readTree, com.mastercard.cas.ds.dto.v1_1_0.AReqDTO.class);
		AReqError aReqError = defaultAreqValidator.validate(aReqDTO);
		assertNull(aReqError);

	}

}
